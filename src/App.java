public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1,"Hương", "Sơn",100000);
        Employee employee2 = new Employee(2,"Ngọc", "Hoa" , 150000);
        //Ghi ra console
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        //Lấy tên đầy đử
        System.out.println("Tên đầy đủ của employee1 là " + employee1.getName());
        System.out.println("Tên đầy đủ của employee2 là " + employee2.getName());
        //Tăng lương cho nhân viên
        System.out.println("Tăng lương cho nhân viên của employee1 là 10% = " + employee1.raiseSalary(10));
        System.out.println("Tăng lương cho nhân viên của employee2 là 20% = " + employee2.raiseSalary(30));
        //Lấy lương theo năm
        System.out.println("Lương theo năm của employee1 là " + employee1.getAnualSalary());
        System.out.println("Lương theo năm của employee2 là " + employee2.getAnualSalary());

    }
}
